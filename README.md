# Project 6: Brevet time calculator service

This project has three parts, on three different ports. On port 5000, we have the table and calculator, which calculates control times based on the distances, and stores them in MongoDB. On port 5001, we have a RESTful service that exposes what is stored in MongoDB. It follows the specification described below. On port 5002, we have a consumer program to use the service provided on port 5001, and to show a few potential representations of the data.

The calculator on port 5000 calculates ACP controle times, using the algorithm described here (https://rusa.org/pages/acp-brevet-control-times-calculator) and implemented here (https://rusa.org/octime_acp.html), using AJAX and Flask. 

It has buttons for submitting and displaying the control times, which is stored using MongoDB. After populating the rows, press the submit button to submit the data. You can submit data multiple times. When you are ready to see your control times, press display.

Importantly, my implementation allows the user to spread their data out, with empty rows between. It will not insert the empty rows into the database, but will only insert the rows with data in them, without throwing an error.

## Specification and examples for RESTful service.

* This service will always show the control points sorted from smallest to largest distance. In all representations, it will show the distances in kilometers.
* The CSV option shows a list of CSV strings, one for each row.
* This service has the following three basic APIs:
    * "http://<host:port>/listAll" should return all open and close times in the database
    * "http://<host:port>/listOpenOnly" should return open times only
    * "http://<host:port>/listCloseOnly" should return close times only

* It has two different representations: one in csv and one in json. JSON is the default representation for the above three basic APIs. 
    * "http://<host:port>/listAll/csv" should return all open and close times in CSV format
    * "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
    * "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format

    * "http://<host:port>/listAll/json" should return all open and close times in JSON format
    * "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
    * "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

* There is a query parameter to get the top "k" open and close times. If the query parameter is an invalid value (such as a negative number, or a number larger than the number of control points), it will default to showing all of the control points. For examples, see below.

    * "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
    * "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
    * "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
    * "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

## Contact info

This project is for CIS 322 at University of Oregon.

Arden Butterfield, abutter2@uoregon.edu