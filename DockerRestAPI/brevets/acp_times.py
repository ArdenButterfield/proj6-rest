"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

# (section start, section end, minimum speed, maximum speed)
location_speeds = ((0, 200, 15, 34), (200, 400, 15, 32), (400, 600, 15, 30),
                   (600, 1000, 11.428, 28), (1000, 1300, 13.333,  26))

# Final control times (at or exceeding brevet distance) are special cases
final_close = {200: 13.5, 300: 20, 400: 27, 600: 40, 1000: 75}
max_dist = 1300

def convert_to_hm(time):
    """Take time in hours as a float, convert (with rounding) to hours and
    minutes as a tuple of ints"""
    hours = int(time)
    minutes = round((time % 1) * 60)
    return (hours, minutes)

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    control_dist_km = round(control_dist_km)
    brevet_dist_km = round(brevet_dist_km)

    time = arrow.get(brevet_start_time)
    for ls in location_speeds:
        start_dist, end_dist, min_speed, max_speed = ls
        if end_dist == brevet_dist_km or end_dist > control_dist_km:
            hours, minutes = convert_to_hm((control_dist_km - start_dist) / max_speed)
            time = time.shift(hours=hours, minutes=minutes)
            break
        hours, minutes = convert_to_hm((end_dist - start_dist) / max_speed)
        time = time.shift(hours=hours, minutes=minutes)

    return time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    time = arrow.get(brevet_start_time)
    if control_dist_km >= brevet_dist_km:
        duration = final_close[brevet_dist_km]
        finish_time = start_time.replace(hours=duration)
        return finish_time.isoformat()
    if control_dist_km == 0:
        return time.shift(hours=1).isoformat()
    if control_dist_km <= 60:
        hours, minutes = convert_to_hm(control_dist_km / 20 + 1) # The French Algorithm
        return time.shift(hours=hours, minutes=minutes).isoformat()
    for ls in location_speeds:
        start_dist, end_dist, min_speed, max_speed = ls
        if end_dist == brevet_dist_km or end_dist > control_dist_km:
            hours, minutes = convert_to_hm((control_dist_km - start_dist) / min_speed)
            time = time.shift(hours=hours, minutes=minutes)
            break
        hours, minutes = convert_to_hm((end_dist - start_dist) / min_speed)
        time = time.shift(hours=hours, minutes=minutes)

    return time.isoformat()