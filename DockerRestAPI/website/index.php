<html>
    <head>
        <title>CIS 322 REST-api: Brevets</title>
    </head>

    <body>
        <h1>Consumer program for brevet calculator</h1>
        <h2>List of all open and close times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
	        $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $open = $b->open;
                $close = $b->close;
                echo "<li>Distance: $km km. Open time: $open Close time: $close</li>";
            }
            ?>
        </ul>
        <h2>List of the first two open and close times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll?top=2');
            $obj = json_decode($json);
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $open = $b->open;
                $close = $b->close;
                echo "<li>Distance: $km km. Open time: $open Close time: $close</li>";
            }
            ?>
        </ul>
        </ul>
        <h2>List of the first two open times</h2>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly?top=2');
            $obj = json_decode($json);
            $obj = json_decode($json);
            $brevets = $obj->brevets;
            foreach ($brevets as $b) {
                $km = $b->km;
                $open = $b->open;
                echo "<li>Distance: $km km. Open time: $open</li>";
            }
            ?>
        </ul>
    </body>
</html>
