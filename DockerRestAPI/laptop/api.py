# Laptop Service
import os
import flask
from flask import request, redirect, url_for, abort
from flask_restful import Resource, Api
from pymongo import MongoClient

import logging
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

#############
#
# API logic stuff
#
#############

def get_dist(result_item):
    """
    Helper function for sorting the control points.
    """
    return float(result_item['km'])

def list_data(list_type="all", list_style="", top=-1):
    """
    list_type: all, OpenOnly, or CloseOnly
    all will include distance in km, open times, and close times.
    OpenOnly will just include km and open times, and CloseOnly
    will just include km and close times.

    list_style: json, csv, or blank (defaults to json)
    csv will send as a list of csv strings, one for each line of info.
    
    top: Number of results to display (will display all of them if
    no value is present, or if the value is outside of the range.
    """
    
    if list_type not in ["all", "OpenOnly", "CloseOnly"]:
        # This should never happen.
        app.logger.debug("Incorrect choice of what to list.")
        abort(404)
    
    if not list_style:
        # Default to json style
        list_style = "json"
    
    app.logger.debug(f"{list_style} list style")
    
    if list_style not in ["json", "csv"]:
        app.logger.debug("Incorrect choice of list format. json and csv are allowed.")
        abort(404)
    _items = db.tododb.find()
    if list_type == "all":
        result = [{'km' : item['km'], 'open': item['open'], 'close': item['close']} for item in _items]
    elif list_type == "OpenOnly":
        result = [{'km' : item['km'], 'open': item['open']} for item in _items]
    elif list_type == "CloseOnly":
        result = [{'km' : item['km'], 'close': item['close']} for item in _items]
    
    # top is the number of results we are going to display.
    # Default to displaying the entire list.
    if top < 0 or top > len(result):
        top = len(result)
    # If illegal options for the number of results are given, display all of the brevets.
    result.sort(key=get_dist)
    app.logger.debug(f"sorted result: {result}")
    if list_style == "json":
        app.logger.debug("sending json result")
        return {"brevets": result[:top]}
    else:
        csv_data = []
        for item_num in range(top):
            item = result[item_num]
            if list_type == "all":
                csv_data.append(f"{item['km']},{item['open']},{item['close']}")
            elif list_type == "OpenOnly":
                csv_data.append(f"{item['km']},{item['open']}")
            elif list_type == "CloseOnly":
                csv_data.append(f"{item['km']},{item['close']}")
        app.logger.debug(f"sending csv result: {csv_data}")
        return csv_data

#############
#
# API classes
#
#############

class ListAll(Resource):
    def get(self):
        top = request.args.get("top", default=-1, type=int)
        return list_data(list_type="all", list_style="", top=top)

class ListAllOptions(Resource):
    def get(self, options):
        top = request.args.get("top", default=-1, type=int)
        return list_data(list_type="all", list_style=options, top=top)

class ListOpenOnly(Resource):
    def get(self):
        top = request.args.get("top", default=-1, type=int)
        return list_data(list_type="OpenOnly", list_style="", top=top)

class ListOpenOnlyOptions(Resource):
    def get(self, options):
        top = request.args.get("top", default=-1, type=int)
        return list_data(list_type="OpenOnly", list_style=options, top=top)

class ListCloseOnly(Resource):
    def get(self):
        top = request.args.get("top", default=-1, type=int)
        return list_data(list_type="CloseOnly", list_style="", top=top)

class ListCloseOnlyOptions(Resource):
    def get(self, options):
        top = request.args.get("top", default=-1, type=int)
        return list_data(list_type="CloseOnly", list_style=options, top=top)
    
api.add_resource(ListAll, '/listAll')
api.add_resource(ListAllOptions, '/listAll/<options>')
api.add_resource(ListOpenOnly, '/listOpenOnly')
api.add_resource(ListOpenOnlyOptions, '/listOpenOnly/<options>')
api.add_resource(ListCloseOnly, '/listCloseOnly')
api.add_resource(ListCloseOnlyOptions, '/listCloseOnly/<options>')

if __name__ == "__main__":
    print("Opening for global access on port {}".format(80))
    app.run(port=80, host="0.0.0.0")
